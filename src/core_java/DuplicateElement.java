package core_java;

import java.util.Arrays;
import java.util.Scanner;

public class DuplicateElement {

	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        // Get the size of the array from the user
	        System.out.print("Enter the size of the array: ");
	        int size = scanner.nextInt();

	        // Create an array with the given size
	        int[] originalArray = new int[size];

	        // Get elements of the array from the user
	        System.out.println("Enter the elements of the array:");
	        for (int i = 0; i < size; i++) {
	            System.out.print("Enter element at index " + i + ": ");
	            originalArray[i] = scanner.nextInt();
	        }

	        // Remove duplicates and display the result
	        int[] arrayWithoutDuplicates = removeDuplicates(originalArray);
	        System.out.println("\nArray after removing duplicates:");
	        System.out.println(Arrays.toString(arrayWithoutDuplicates));

	        // Close the scanner to avoid resource leak
	        scanner.close();
	    }

	    // Function to remove duplicates from an array
	    private static int[] removeDuplicates(int[] array) {
	        // Sort the array to group duplicate elements together
	        Arrays.sort(array);

	        // Count the number of unique elements
	        int uniqueCount = 0;
	        for (int i = 0; i < array.length - 1; i++) {
	            if (array[i] != array[i + 1]) {
	                uniqueCount++;
	            }
	        }

	        // Create a new array with unique elements
	        int[] resultArray = new int[uniqueCount + 1];
	        resultArray[0] = array[0];
	        int index = 1;

	        for (int i = 1; i < array.length; i++) {
	            if (array[i] != array[i - 1]) {
	                resultArray[index++] = array[i];
	            }
	        }

	        return resultArray;
	    }
	    
}
