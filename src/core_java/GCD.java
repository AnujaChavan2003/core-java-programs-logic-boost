package core_java;

import java.util.Scanner;

public class GCD {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get input from the user
        System.out.print("Enter the first number: ");
        int num1 = Math.abs(scanner.nextInt()); // Ensure non-negative value

        System.out.print("Enter the second number: ");
        int num2 = Math.abs(scanner.nextInt()); // Ensure non-negative value

        // Calculate and display the GCD
        int gcd = calculateGCD(num1, num2);
        System.out.println("The GCD of " + num1 + " and " + num2 + " is: " + gcd);

        // Close the scanner to avoid resource leak
        scanner.close();
    }

    // Function to calculate the GCD using the Euclidean algorithm
    private static int calculateGCD(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
