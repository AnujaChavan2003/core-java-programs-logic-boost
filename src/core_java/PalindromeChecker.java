package core_java;

import java.util.Scanner;

public class PalindromeChecker {

	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        // Get input from the user
	        System.out.print("Enter a number: ");
	        int number = scanner.nextInt();

	        // Check if the number is a palindrome in decimal
	        if (isPalindromeDecimal(number)) {
	            System.out.println(number + " is a palindrome in decimal.");
	        } else {
	            System.out.println(number + " is not a palindrome in decimal.");
	        }

	        // Check if the binary representation of the number is a palindrome
	        String binaryRepresentation = Integer.toBinaryString(number);
	        if (isPalindromeString(binaryRepresentation)) {
	            System.out.println(number + " is a palindrome in binary.");
	        } else {
	            System.out.println(number + " is not a palindrome in binary.");
	        }

	        // Close the scanner to avoid resource leak
	        scanner.close();
	    }

	    // Function to check if a number is a palindrome in decimal
	    private static boolean isPalindromeDecimal(int num) {
	        int originalNum = num;
	        int reverse = 0;

	        while (num > 0) {
	            int digit = num % 10;
	            reverse = reverse * 10 + digit;
	            num /= 10;
	        }

	        return originalNum == reverse;
	    }

	    // Function to check if a string is a palindrome
	    private static boolean isPalindromeString(String str) {
	        int left = 0;
	        int right = str.length() - 1;

	        while (left < right) {
	            if (str.charAt(left) != str.charAt(right)) {
	                return false;
	            }
	            left++;
	            right--;
	        }

	        return true;
	    }
	    
}
