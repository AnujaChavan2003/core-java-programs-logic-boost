package core_java;

import java.util.Scanner;

public class LargestElement {


	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        // Get the size of the array from the user
	        System.out.print("Enter the size of the array: ");
	        int size = scanner.nextInt();

	        // Create an array with the given size
	        int[] array = new int[size];

	        // Get elements of the array from the user
	        System.out.println("Enter the elements of the array:");
	        for (int i = 0; i < size; i++) {
	            System.out.print("Enter element at index " + i + ": ");
	            array[i] = scanner.nextInt();
	        }

	        // Find the largest element in the array
	        int largest = array[0];
	        for (int i = 1; i < size; i++) {
	            if (array[i] > largest) {
	                largest = array[i];
	            }
	        }

	        System.out.println("The largest element in the array is: " + largest);
	    }
	}

	

