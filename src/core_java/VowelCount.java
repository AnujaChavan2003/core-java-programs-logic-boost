package core_java;

import java.util.Scanner;

public class VowelCount {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get input string from the user
        System.out.print("Enter a string: ");
        String input = scanner.nextLine();

        // Count the number of vowels and consonants
        int vowelsCount = countVowels(input);
        int consonantsCount = countConsonants(input);

        // Display the result
        System.out.println("Number of vowels: " + vowelsCount);
        System.out.println("Number of consonants: " + consonantsCount);

        // Close the scanner to avoid resource leak
        scanner.close();
    }

    // Function to count the number of vowels in a string
    private static int countVowels(String str) {
        int count = 0;
        str = str.toLowerCase(); // Convert the string to lowercase for case-insensitive matching

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                count++;
            }
        }

        return count;
    }

    // Function to count the number of consonants in a string
    private static int countConsonants(String str) {
        int count = 0;
        str = str.toLowerCase(); // Convert the string to lowercase for case-insensitive matching

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch >= 'a' && ch <= 'z' && !(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')) {
                count++;
            }
        }

        return count;
    }
    
}
